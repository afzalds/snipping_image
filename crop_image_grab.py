# import the necessary packages
import cv2
import argparse
from PIL import Image
from pytesseract import image_to_string
import sys
from io import BytesIO
import pyscreenshot as ImageGrab
import numpy as np
# initialize the list of reference points and boolean indicating
# whether cropping is being performed or not
refPt = []
cropping = False

def click_and_crop(event, x, y, flags, param):
	# grab references to the global variables
	global refPt, cropping
 
	# if the left mouse button was clicked, record the starting
	# (x, y) coordinates and indicate that cropping is being
	# performed
	if event == cv2.EVENT_LBUTTONDOWN:
		refPt = [(x, y)]
		cropping = True
 
	# check to see if the left mouse button was released
	elif event == cv2.EVENT_LBUTTONUP:
		# record the ending (x, y) coordinates and indicate that
		# the cropping operation is finished
		refPt.append((x, y))
		cropping = False
 
		# draw a rectangle around the region of interest
		cv2.rectangle(image, refPt[0], refPt[1], (0, 255, 0), 2)
		#width = image.shape[0]
		cv2.imshow("image", image)
"""
# construct the argument parser and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to the image")
args = vars(ap.parse_args())
"""
while True:
    key = input()
    while(key != "s"):
        img = ImageGrab.grab()
        print(type(img))
        img_np = np.array(img)
        frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
        image = frame.copy() 
        key = input()
    #cv2.imshow("screen",s_image)
    #cv2.waitKey(0)
    #print (type(s_image))
    #buffer = BytesIO()
    #slic_image = Image.fromarray(s_image)
    #slic_image.save(buffer,format="JPEG")
    #image = Image.open(slic_image)
    #image.show()
#    cv2.imshow("screen",frame)
    if key == "s":
        clone = image.copy()
        cv2.namedWindow("image")
        cv2.setMouseCallback("image", click_and_crop)
        text = ""
        while True:
			# display the image and wait for a keypress
            cv2.imshow("image", image)
            key = cv2.waitKey(1) & 0xFF

			# if the 'r' key is pressed, reset the cropping region
            if key == ord("r"):
                image = clone.copy()
                cv2.waitKey(0)
			# if the 'c' key is pressed, break from the loop
            elif key == ord("c"):
                image = clone.copy()
                if len(refPt) == 2:
                    roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
					######
                    buffered = BytesIO()
                    slice_image = Image.fromarray(roi)
                    slice_image.save(buffered,format="JPEG")
					######
					#cv2.imwrite("abc.jpg",roi)
					#img = Image.open("abc.jpg")
                    img = Image.open(buffered)
                    text = image_to_string(roi,lang='eng')
                    print(text)
            elif key == ord("q"):
                cv2.destroyAllWindows()
                break
    elif key == "q":
        break
# load the image, clone it, and setup the mouse callback function
# image = cv2.imread(args["image"])
# clone = image.copy()
# cv2.namedWindow("image")
# cv2.setMouseCallback("image", click_and_crop)
 
# keep looping until the 'q' key is pressed
'''
text = ""
while True:
	# display the image and wait for a keypress
	cv2.imshow("image", image)
	key = cv2.waitKey(1) & 0xFF

	# if the 'r' key is pressed, reset the cropping region
	if key == ord("r"):
		image = clone.copy()
		cv2.waitKey(0)
	# if the 'c' key is pressed, break from the loop
	elif key == ord("c"):
		image = clone.copy()
		if len(refPt) == 2:
			roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
			######
			buffered = BytesIO()
			slice_image = Image.fromarray(roi)
			slice_image.save(buffered,format="JPEG")
			######
			#cv2.imwrite("abc.jpg",roi)
			#img = Image.open("abc.jpg")
			img = Image.open(buffered)
			text = image_to_string(roi,lang='eng')
			print(text)
	elif key == ord("q"):
		break
'''

# if there are two reference points, then crop the region of interest
# from teh image and display it
#if len(refPt) == 2:
#	roi = clone[refPt[0][1]:refPt[1][1], refPt[0][0]:refPt[1][0]]
#	cv2.imwrite("abc.jpg",roi)
#	img = Image.open("abc.jpg")
#	text = image_to_string(roi,lang='eng')
#	print(text)
	#cv2.imshow("ROI", roi)
    #img = Image.open(image_name)
    #text = image_to_string(img,lang='eng')
 
# close all open windows
cv2.destroyAllWindows()

#python crop_image.py --image 511763575.tif_15.png
#511763575.tif_15.png

'''
		height = image.shape[1]
		val = float("%.2f"%(500/height))
		height = int(image.shape[1]*val)
		width = int(image.shape[0]*val)
		dim = (width,height)
		image = cv2.resize(image, dim, interpolation = cv2.INTER_AREA)
'''