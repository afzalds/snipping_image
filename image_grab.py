import pyscreenshot as ImageGrab
import cv2
import numpy as np


while True:
    img = ImageGrab.grab()
    img_np = np.array(img)
    frame = cv2.cvtColor(img_np, cv2.COLOR_BGR2RGB)
    cv2.imshow("screeen",frame)
    key = cv2.waitKey(1) & 0xFF     
    if key == ord("q"):
        cv2.waitKey(0)
        break
cv2.destroyAllWindows()