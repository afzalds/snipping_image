# Simple enough, just import everything from tkinter.
from tkinter import *
import pyautogui

#download and install pillow:
# http://www.lfd.uci.edu/~gohlke/pythonlibs/#pillow
from PIL import Image, ImageTk


# Here, we are creating our class, Window, and inheriting from the Frame
# class. Frame is a class from the tkinter module. (see Lib/tkinter/__init__)
class Window(Frame):

    # Define settings upon initialization. Here you can specify
    def __init__(self, master=None):
        
        # parameters that you want to send through the Frame class. 
        Frame.__init__(self, master)   

        #reference to the master widget, which is the tk window                 
        self.master = master

        #with that, we want to then run init_window, which doesn't yet exist
        self.init_window()

    #Creation of init_window
    def init_window(self):

        # changing the title of our master widget      
        self.master.title("GUI")

        # allowing the widget to take the full space of the root window
        self.pack(fill=BOTH, expand=1)

        # creating a menu instance
        menu = Menu(self.master)
        self.master.config(menu=menu)

        # create the file object)
        file = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        file.add_command(label="New")
        file.add_command(label="Crop", command=self.crop_screen)
        file.add_command(label="Open")  
        file.add_command(label="Save")  
        file.add_command(label="Save as...")  
        file.add_command(label="Close")  
        
        file.add_separator()          
        file.add_command(label="Exit", command=self.client_exit)

        #added "file" to our menu
        menu.add_cascade(label="File", menu=file)


        # create the file object)
        edit = Menu(menu)

        # adds a command to the menu option, calling it exit, and the
        # command it runs on event is client_exit
        edit.add_command(label="Show Img", command=self.showImg)
        edit.add_command(label="Show Text", command=self.showText)

        #added "file" to our menu
        menu.add_cascade(label="Edit", menu=edit)

    def crop_screen(self):
        image = pyautogui.screenshot()
        pass


    def showImg(self):
        same = False
        #n can't be zero, recommend 0.25-4
        n=2
#        image = Image.open(path)
        image = Image.open("D_5650243.png")
        print(image)
        input()
        imageSizeWidth, imageSizeHeight = image.size
        newImageSizeWidth = int(imageSizeWidth*n)
        if same:
            newImageSizeHeight = int(imageSizeHeight*n) 
        else:
            newImageSizeHeight = int(imageSizeHeight/n) 

        image = image.resize((newImageSizeWidth, newImageSizeHeight), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(image)


        #image = image.resize((400, 400), Image.ANTIALIAS) ## The (250, 250) is (height, width)
        #self.pw.pic = ImageTk.PhotoImage(image)
        #render = ImageTk.PhotoImage(image)
        Canvas1 = Canvas(root)

        Canvas1.create_image(newImageSizeWidth/2,newImageSizeHeight/2,image = img)      
        Canvas1.config(width = newImageSizeWidth, height = newImageSizeHeight)
        Canvas1.pack(side=LEFT,expand=True,fill=BOTH)
        # labels can be text or images


    def showText(self):
        text = Label(self, text="Hey there good lookin!")
        text.pack()
        

    def client_exit(self):
        exit()


# root window created. Here, that would be the only window, but
# you can later have windows within windows.
root = Tk()
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
height_of_window = screen_height - 100
width_of_window = screen_width - 100
x_coor = (screen_width/2) - (width_of_window/2)
y_coor = (screen_height/2) - (height_of_window/2)
root.geometry("%dx%d+%d+%d" %(width_of_window,height_of_window,x_coor,y_coor))
#root.geometry("400x300")

#creation of an instance
app = Window(root)


#mainloop 
root.mainloop() 